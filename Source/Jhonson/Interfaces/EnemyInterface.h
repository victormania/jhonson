// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "EnemyInterface.generated.h"

UINTERFACE(MinimalAPI)
class UEnemyInterface : public UInterface
{
	GENERATED_BODY()
};

class JHONSON_API IEnemyInterface
{
	GENERATED_BODY()

public:
	virtual TArray<class ATargetPoint*> GetWaypoints() = 0;
	virtual bool GetAtack() = 0;
	virtual void SetAtack(bool atack) = 0;

	virtual bool GetType() = 0;
};
