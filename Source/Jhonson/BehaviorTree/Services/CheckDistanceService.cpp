// Fill out your copyright notice in the Description page of Project Settings.

#include "CheckDistanceService.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Interfaces/EnemyInterface.h"
#include "AIController.h"


void UCheckDistanceService::TickNode(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory, float DeltaSeconds) 
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	UBlackboardComponent* blackboardComp = OwnerComp.GetBlackboardComponent();
	UObject* targetObject = blackboardComp->GetValueAsObject(targetKey.SelectedKeyName);
	AActor* targetActor = Cast<AActor>(targetObject);

	AAIController* aiController = Cast<AAIController>(OwnerComp.GetAIOwner());
	
	IEnemyInterface* enemy = Cast<IEnemyInterface>(aiController->GetPawn());
	

	if (targetActor) {
		FVector controllerdPawnLocation = OwnerComp.GetOwner()->GetActorLocation();
		FVector targetActorLocation = targetActor->GetActorLocation();

		FVector diff = controllerdPawnLocation - targetActorLocation;
		float dist = diff.Size();
		if (dist<= minDistance) {
			blackboardComp->SetValueAsBool(atackKey.SelectedKeyName, true);
			enemy->SetAtack(true);
		}
		else {
			blackboardComp->SetValueAsBool(atackKey.SelectedKeyName, false);
			enemy->SetAtack(false);
		}
	}

}

