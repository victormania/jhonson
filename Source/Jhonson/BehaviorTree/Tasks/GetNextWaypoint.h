// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "GetNextWaypoint.generated.h"

struct FNextWaypointData 
{
	int currentIndex = -1;
};

UCLASS()
class JHONSON_API UGetNextWaypoint : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnyWhere) FBlackboardKeySelector bbtarget;

	uint16 GetInstanceMemorySize() const override;
	
protected:
	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory) override;
	
};
