// Fill out your copyright notice in the Description page of Project Settings.

#include "GetNextWaypoint.h"
#include "AIController.h"
#include "Engine//TargetPoint.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Interfaces/EnemyInterface.h"


uint16 UGetNextWaypoint::GetInstanceMemorySize() const
{
	return sizeof(FNextWaypointData);
}

EBTNodeResult::Type UGetNextWaypoint::ExecuteTask(UBehaviorTreeComponent & OwnerComp, uint8 * NodeMemory)
{
	
	AAIController* aiController = Cast<AAIController>(OwnerComp.GetAIOwner());
	if (!aiController) {
		return EBTNodeResult::Type::Failed;
	}
	IEnemyInterface* enemy = Cast<IEnemyInterface>(aiController->GetPawn());
	if (!enemy) {
		return EBTNodeResult::Type::Failed;
	}
	TArray<ATargetPoint*> waypoints = enemy->GetWaypoints();
	FNextWaypointData* data = (FNextWaypointData*)NodeMemory;
	data->currentIndex++;

	if (data->currentIndex < 0 || data->currentIndex >= waypoints.Num()) {
		data->currentIndex = 0;
	}

	
	ATargetPoint* currenteWayPints = waypoints[data->currentIndex];

	UBlackboardComponent* blackboard = OwnerComp.GetBlackboardComponent();
	blackboard->SetValueAsVector(bbtarget.SelectedKeyName,currenteWayPints->GetActorLocation());
	return EBTNodeResult::Type::Succeeded;
}