// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"
#include "JhonsonCharacter.h"
#include "Enemies/BossEnemy.h"
#include "GameController.generated.h"

UCLASS()
class JHONSON_API AGameController : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "JHONSON")
		bool GetGamerOver();

	UPROPERTY(EditAnywhere, Category = "GameWidgets")
		TSubclassOf<class UUserWidget> GameOverTextWidget;

	TWeakObjectPtr<class UUserWidget> pGameOverWidget;
	
	void OpenWidgetGameOver();

	TWeakObjectPtr<AJhonsonCharacter> player;
	TWeakObjectPtr<ABossEnemy> boss;

	bool gameOver;

	void InitObjectPtr();
};
