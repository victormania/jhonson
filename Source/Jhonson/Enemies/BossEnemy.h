// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interfaces/EnemyInterface.h"
#include "GameFramework/Character.h"
#include "BossEnemy.generated.h"

UCLASS()
class JHONSON_API ABossEnemy : public ACharacter, public IEnemyInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABossEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		virtual void OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

	TSubclassOf<AActor> ProjectileClass;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnyWhere)
		TArray<class ATargetPoint*>waypoints;

	UPROPERTY(EditAnyWhere)
		bool atack;

	TArray<class ATargetPoint*> GetWaypoints() override;

	UFUNCTION(BlueprintCallable)
		bool GetAtack() override;

	UFUNCTION(BlueprintCallable)
		void SetAtack(bool atackChange) override;
	
	int life;
	int GetLife();
	bool GetType();
};
