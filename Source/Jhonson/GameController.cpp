// Fill out your copyright notice in the Description page of Project Settings.

#include "GameController.h"
#include "TextWidgetTypes.h"
#include <Runtime/Engine/Classes/Engine/Engine.h>
#include "EngineUtils.h"
#include "JhonsonCharacter.h"
#include "Enemies/BossEnemy.h"

// Sets default values
AGameController::AGameController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	gameOver = false;
	
}

// Called when the game starts or when spawned
void AGameController::BeginPlay()
{
	Super::BeginPlay();
	InitObjectPtr();
}

// Called every frame
void AGameController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!gameOver) {
		
		if (boss.Get()->GetLife()<=0) {
			gameOver = true;
			boss.Get()->Destroy();
			OpenWidgetGameOver();
		}
	}
}

bool AGameController::GetGamerOver()
{
	return this->gameOver;
}

void AGameController::OpenWidgetGameOver()
{
	if (GameOverTextWidget) // Check if the Asset is assigned in the blueprint.
	{
		// Create the widget and store it.
		pGameOverWidget = CreateWidget<UUserWidget>(GetGameInstance(), GameOverTextWidget);

		// now you can use the widget directly since you have a reference for it.
		// Extra check to  make sure the pointer holds the widget.
		if (pGameOverWidget.IsValid())
		{
			// let add it to the view port
			pGameOverWidget->AddToViewport();
		}
	}
}

void AGameController::InitObjectPtr()
{
	for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		if (ActorItr->IsA(AJhonsonCharacter::StaticClass()))
		{
			// Conversion to smart pointer
			player = Cast<AJhonsonCharacter>(*ActorItr);
			break;
		}
	}

	for (TActorIterator<AActor> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		if (ActorItr->IsA(ABossEnemy::StaticClass()))
		{
			// Conversion to smart pointer
			boss = Cast<ABossEnemy>(*ActorItr);
			break;
		}
	}
}

